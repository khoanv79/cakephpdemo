<!-- File: src/Template/Articles/index.ctp -->

<fieldset>
    <legend><h4> New Post </h4></legend>
    <?= $this->Form->create(null, ['url' => ['action' => 'add'], 'enctype' => 'multipart/form-data']) ?>
    <?= $this->Form->textarea('caption', ['rows' => '3', 'cols' => '5'], array('class' => 'box-comment')); ?>
    <?= $this->Form->input('image', ['type' => 'file']) ?>
    <?= $this->Form->button('Post'); ?>
    <?= $this->Form->end() ?>
    <br>
    <legend><h4> Newsfeed </h4></legend>
    <br>
    <br>
    <?php
    foreach ($posts as $post): ?>
        <legend><?= $post->c['username'] ?></legend>
        <article>
            <?= $post->caption ?>
            <br>
            <?php if ($post->image_link): ?>
                <?= $this->Html->image('/img/' . $post->image_link, array('alt' => $post->image_link, 'class' => 'display-image')); ?>
            <?php endif; ?>
            <br>
            <?= array_key_exists($post->id, $likes) ? $likes[$post->id] : 0; ?>
            <?php if (!in_array($post->id, $check_likes)): ?>
                <?= $this->Form->create(null, ['url' => ['action' => 'like']]) ?>
                <?= $this->Form->hidden('post_id', array('value' => $post->id)); ?>
                <?= $this->Form->button('Like'); ?>
            <?php else: ?>
                <?= $this->Form->create(null, ['url' => ['action' => 'unlike']]) ?>
                <?= $this->Form->hidden('post_id', array('value' => $post->id)); ?>
                <?= $this->Form->button('Unlike'); ?>
            <?php endif; ?>
            <br>
            <?php

            if (array_key_exists($post->id, $comments)) {
                foreach ($comments[$post->id] as $v) {
                    echo '<b>' . $v['username'] . '</b>' . ': ' . $v['content'];
                    echo '<br>';
                }
            }
            ?>
            <?= $this->Form->end() ?>
            <br>
            <?= $this->Form->create(null, ['url' => ['action' => 'comment']]) ?>
            <?= $this->Form->hidden('post_id', array('value' => $post->id)); ?>
            <?= $this->Form->textarea('content', ['rows' => '3', 'cols' => '5'], array('class' => 'box-comment')); ?>
            <?= $this->Form->button('Comment'); ?>
            <?= $this->Form->end() ?>
        </article>
        <br>
        <br>
    <?php endforeach; ?>
</fieldset>
