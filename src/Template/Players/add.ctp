<?php echo $this->Html->css('PlayerIndex'); ?>

<h1>Add Player</h1>
<?php
echo $this->Form->create($player, ['enctype' => 'multipart/form-data']);
echo $this->Form->input('name');
echo $this->Form->input('age', array(
    'label' => 'Date of Birth',
    'type' => 'date',
    'dateFormat' => 'D M Y',
    'empty' => false,
    'minYear' => '1920',
    'maxYear' => date('Y')

));
echo 'Position<br>';
echo $this->Form->select(
    'position',
    $position);
echo $this->Form->input('image', ['type' => 'file']);
echo $this->Form->button(__('Save Player'));
echo $this->Form->end();
?>
