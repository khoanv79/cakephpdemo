<h1>Edit Player</h1>
<?php
echo $this->Form->create($player);
echo $this->Form->input('name');
echo $this->Form->input('age');
echo $this->Form->select(
    'position',
    $position);
echo $this->Form->button(__('Save Player'));
echo $this->Form->end();
?>
