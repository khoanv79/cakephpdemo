<?php echo $this->Html->css('PlayerIndex'); ?>

<table style="">
    <tr>
        <th class="col-no">No.</th>
        <th>Image</th>
        <th>Name</th>
        <th>Age</th>
        <th>Position</th>
        <th class="sub-col"></th>
        <th class="sub-col"></th>
    </tr>
    <?php foreach ($info as $each): ?>
        <tr>
            <td><?= $each->id ?></td>
            <td><?= $this->Html->image('/img/' . $each->image, array('alt' => $each->name, 'class' => 'display-image')); ?></td>
            <td><b><?= $this->Html->link($each->name, ['action' => 'view', $each->id]) ?></b></td>
            <td><?php echo $each->age->diff(new DateTime())->y ?></td>
            <td><?= $position[$each->position] ?></td>
            <td><?= $this->Html->link("Edit", ['action' => 'edit', $each->id]) ?></td>
            <td> <?= $this->Form->postLink(
                    'Delete',
                    ['action' => 'delete', $each->id],
                    ['confirm' => 'Are you sure?'])
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<?= $this->Html->link('Add Player', ['action' => 'add']) ?>
