<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Van Khoa
 * Date: 1/25/2016
 * Time: 1:47 PM
 */
namespace App\Model\Table;

use Cake\Validation\Validator;

use Cake\ORM\Table;

class PlayersTable extends Table
{
    public function initialize(array $config)
    {
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('name')
            ->requirePresence('name')
            ->notEmpty('age')
            ->requirePresence('age')
            ->notEmpty('image')
            ->requirePresence('image');
        return $validator;
    }


    public function getNameById($id = null)
    {
        return $this->find('all')->where(['id' => $id])->first()->name;
    }

}