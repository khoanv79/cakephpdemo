<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Van Khoa
 * Date: 1/26/2016
 * Time: 1:48 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{

    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('username', 'A username is required')
            ->notEmpty('password', 'A password is required')
            ->notEmpty('role', 'A role is required');
    }

}