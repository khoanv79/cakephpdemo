<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Van Khoa
 * Date: 1/26/2016
 * Time: 3:02 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class LikesTable extends Table
{
    public function getNumberLikes()
    {
        $queryLike = $this->find('all');
        $queryLike->select([
            'post_id',
            'count' => $queryLike->func()->count('*')
        ])
            ->group('post_id');

        foreach ($queryLike as $i => $v) {
            $array_likes[$v['post_id']] = $v['count'];
        }
        return $array_likes;
    }

    public function checkLikes($user_id)
    {
        $likes_list = $this->find('all')
            ->where(['Likes.user_id' => $user_id]);
        foreach ($likes_list as $v) {
            $list[] = $v->post_id;
        }
        return $list;
    }
}