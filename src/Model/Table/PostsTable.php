<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Van Khoa
 * Date: 1/26/2016
 * Time: 3:02 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PostsTable extends Table
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('caption')
            ->requirePresence('caption');
        return $validator;
    }

    public function getAllPosts()
    {
        return $this->find('all',
            array('fields' => array(
                'Posts.id',
                'Posts.image_link',
                'Posts.caption',
                'Posts.type',
                'c.username',
            )))
            ->join([
                'table' => 'users',
                'alias' => 'c',
                'type' => 'LEFT',
                'conditions' => 'c.id = posts.user_id',
            ])
            ->order('Posts.id desc');
    }
}