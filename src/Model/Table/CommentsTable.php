<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Van Khoa
 * Date: 1/26/2016
 * Time: 3:02 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class CommentsTable extends Table
{
    public function getAllComments()
    {
        $comment_list = $this->find('all',
            array('fields' => array(
                'Comments.id',
                'Comments.post_id',
                'Comments.content',
                'c.username',
            )))
            ->join([
                'table' => 'users',
                'alias' => 'c',
                'type' => 'LEFT',
                'conditions' => 'c.id = comments.user_id',
            ]);
        foreach ($comment_list as $k => $v) {
            $comments[$v->post_id][] = array('content' => $v->content, 'username' => $v->c['username']);
        }
        return $comments;
    }
}