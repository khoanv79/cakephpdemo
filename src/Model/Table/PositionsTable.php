<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Van Khoa
 * Date: 1/25/2016
 * Time: 1:47 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;

class PositionsTable extends Table
{
    public function initialize(array $config)
    {
    }

    public function getPositionById($id = null)
    {
        return $this->find('all')->where(['id' => $id])->first()->name;
    }

}