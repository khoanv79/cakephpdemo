<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Van Khoa
 * Date: 1/26/2016
 * Time: 2:12 PM
 */
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

class User extends Entity
{

    // Make all fields mass assignable except for primary key field "id".
    protected $_accessible = ['*' => true];


    // ...

    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

    // ...
}