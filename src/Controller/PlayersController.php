<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Van Khoa
 * Date: 1/25/2016
 * Time: 1:21 PM
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Filesystem\File;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;


class PlayersController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Flash'); // Include the FlashComponent
        $this->loadModel('Positions');
    }

    public function index()
    {
        $info = $this->Players->find('all');
        $this->set(compact('info'));
        $position = $this->Positions->find('list')->toArray();
        $this->set('position', $position);
    }

    public function view($id = null)
    {
        $info = $this->Players->get($id);
        $this->set(compact('info'));
    }

    public function add()
    {
        $players = TableRegistry::get('Players');
        $position = $this->Positions->find('list')->toArray();
        $this->set('position', $position);
        $player = $this->Players->newEntity();
        if ($this->request->is('post')) {
            $file = $this->request->data['image'];
            $this->request->data['age'] = Time::createFromFormat('Y-m-d', implode('-', array_values($this->request->data['age'])));
            $extension = substr(strtolower(strrchr($this->request->data['image']['name'], '.')), 1);
            $player = $this->Players->patchEntity($player, $this->request->data);
            $insert = $this->Players->save($player);
            if ($insert) {
                move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/' . $insert->id . '.' . $extension);
                $players->query()->update()
                    ->set(['image' => $insert->id . '.' . $extension])
                    ->where(['id' => $insert->id])
                    ->execute();
                $this->Flash->success(__('Your player {0} has been added.', $insert->name));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your player.'));
        }
        $this->set('player', $player);
    }

    public function edit($id = null)
    {
        $position = $this->Positions->find('list')->toArray();
        $this->set('position', $position);
        $player = $this->Players->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Players->patchEntity($player, $this->request->data);
            if ($this->Players->save($player)) {
                $this->Flash->success(__('Your player {0} has been updated.', $player->name));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to update your player.'));
        }

        $this->set('player', $player);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $player = $this->Players->get($id);
        if ($this->Players->delete($player)) {
            $this->Flash->success(__('Your player {0} has been deleted.', $player->name));
            return $this->redirect(['action' => 'index']);
        }
    }


}