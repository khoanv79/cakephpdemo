<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Van Khoa
 * Date: 1/26/2016
 * Time: 3:05 PM
 */

namespace App\Controller;

use Cake\ORM\TableRegistry;

class PostsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Flash'); // Include the FlashComponent
        $this->loadModel('Comments');
        $this->loadModel('Likes');
    }

    public function index()
    {
        $this->set('posts', $this->Posts->getAllPosts());
        $this->set('comments', $this->Comments->getAllComments());
        $this->set('likes', $this->Likes->getNumberLikes());
        $this->set('check_likes', $this->Likes->checkLikes($this->Auth->user('id')));
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if ($data['caption'] == null) {
                $this->redirect(array('action' => 'index'));
            }
            $data['user_id'] = $this->Auth->user('id');
            $file = $data['image'];
            $extension = substr(strtolower(strrchr($file['name'], '.')), 1);

            $postsTable = TableRegistry::get('Posts');
            $post = $postsTable->newEntity();
            $post = $this->Posts->patchEntity($post, $data);
            $insert = $postsTable->save($post);
            if ($insert) {
                move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/' . $insert->id . '.' . $extension);
                $postsTable->query()->update()
                    ->set(['image_link' => $insert->id . '.' . $extension])
                    ->where(['id' => $insert->id])
                    ->execute();
            };
            $this->redirect(array('action' => 'index'));
        }
    }

    public function view($id = null)
    {
        $post = $this->Posts->get($id);
        $this->set(compact('post'));
    }

    public function comment()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $data['user_id'] = $this->Auth->user('id');
            $commentsTable = TableRegistry::get('Comments');
            $comment = $commentsTable->newEntity();
            $comment = $this->Comments->patchEntity($comment, $data);
            $commentsTable->save($comment);
            $this->redirect(array('action' => 'index'));
        }
    }

    public function like()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $data['user_id'] = $this->Auth->user('id');
            $likesTable = TableRegistry::get('Likes');
            $like = $likesTable->newEntity();
            $like = $this->Likes->patchEntity($like, $data);
            $likesTable->save($like);
            $this->redirect(array('action' => 'index'));
        }
    }

    public function unlike()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $query = $this->Likes->query();
            $query->delete()
                ->where(['post_id' => $data['post_id'], 'user_id' => $this->Auth->user('id')])
                ->execute();
            $this->redirect(array('action' => 'index'));
        }
    }
}